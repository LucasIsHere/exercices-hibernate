create database bdd_exos_hibernate;
use bdd_exos_hibernate;

-- select * from produit;
-- select * from image;
-- select * from commentaire;
-- select * from commande;
-- select * from composer;

-- select * from produit
-- inner join commentaire on produit.id = commentaire.produit_id
-- where note>=4;

# TP1 Hibernate
select * from patient;
select * from dossier_medical;
select * from fiche_de_soin;
select * from fiche_paiement;
select * from fiche_consultation;
select * from consultation;
select * from prescription;
select * from operation_analyse;
