package org.example.services;

import org.example.dao.IDAOProduit;
import org.example.entities.Commande;
import org.example.entities.Commentaire;
import org.example.entities.Image;
import org.example.entities.Produit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class ServiceProduit implements IDAOProduit {

    private StandardServiceRegistry registre;
    private SessionFactory sessionFactory;
    private Session session;

    public ServiceProduit() {
        registre = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        session = sessionFactory.openSession();
    }

    @Override
    public Boolean createProduit(Produit p) {
        session.save(p);
        return true;
    }

    @Override
    public Produit getProduit(int id) {
        Produit p = session.get(Produit.class, id);
        return p;
    }

    @Override
    public Boolean deleteProduit(Produit p) {
        session.beginTransaction();
        session.delete(p);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Boolean updateProduit(Produit p) {
        session.beginTransaction();
        session.update(p);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public List<Produit> getAllProduits() {
        List<Produit> produits = session.createQuery("from Produit").list();
        return produits;
    }

    @Override
    public List<Produit> filterByPriceOver(Double prix) {
        List<Produit> produits = session.createQuery("from Produit where prix>100").list();
        return produits;
    }

    @Override
    public List<Produit> filterByDate(Date d1, Date d2) {
        List<Produit> produits = session
                .createQuery("from Produit where dateAchat between :startDate and :endDate ")
                .setParameter("startDate", d1)
                .setParameter("endDate", d2).list();
        return produits;
    }

    @Override
    public List fonction1Exo3(Date d1, Date d2) throws Exception {
        if (d1.before(d2)) {
            List produits = session
                    .createQuery("from Produit where dateAchat >= :d1 and dateAchat <= :d2")
                    .setParameter("d1", d1)
                    .setParameter("d2", d2).list();
            return produits;
        }
        throw new Exception("Date error.");
    }

    @Override
    public void fonction2Exo3(Integer stock) {
        List list = session
                .createQuery("select id, reference from Produit where stock<=?1")
                .setParameter(1, stock)
                .list();
        for (Object o : list) {
            Object[] res = (Object[]) o;
            System.out.println("Numéro produit : " + res[0] + ", référence : " + res[1]);
        }
    }

    @Override
    public Long fonction1Exo4() {
        Long resultat = (Long) session
                .createQuery("select sum(stock) from Produit where marque='Bic'")
                .uniqueResult();
        return resultat;
    }

    @Override
    public Double fonction2Exo4() {
        Double moyenne = (Double) session
                .createQuery("select avg(prix) from Produit")
                .uniqueResult();
        return moyenne;
    }

    @Override
    public List fonction3Exo4(List<Object> marques) {
        List listProduits = session
                .createQuery("from Produit where marque in :marquesVoiture")
                .setParameter("marquesVoiture", marques)
                .list();
        return listProduits;
    }

    @Override
    public int fonction4Exo4(String marque) {
        session.beginTransaction();
        int res = session
                .createQuery("delete from Produit where marque=?1")
                .setParameter(1, marque)
                .executeUpdate();
        session.getTransaction().commit();
        return res;
    }

    @Override
    public boolean addNewImage(Image img, int id) {
        boolean result = false;
        Produit produit = this.getProduit(id);
        session.beginTransaction();
        if (produit != null) {
            img.setProduit(produit);
            session.save(img);
            result = true;
        }
        session.getTransaction().commit();
        return result;
    }

    @Override
    public boolean addNewComment(Commentaire com, int id) {
        boolean result = false;
        Produit produit = this.getProduit(id);
        session.beginTransaction();
        if (produit != null) {
            com.setProduit(produit);
            session.save(com);
            result = true;
        }
        session.getTransaction().commit();
        return result;
    }

    @Override
    public List filterProductsByGrade(int note) {
        List produits = session
                .createSQLQuery("select * from Produit " +
                        "inner join Commentaire on Produit.id = Commentaire.produit_id " +
                        "where note>=:note")
                .setParameter("note", note).list();
//        Query<Produit> produitQuery = session
//                .createQuery("select distinct produit from Commentaire where note >=:note");
//        produitQuery.setParameter("note", note);
//        List produits = produitQuery.list();
        return produits;
    }

    @Override
    public boolean createCommande(List<Produit> produits) {
        boolean result = false;
        if (produits != null) {
            session.beginTransaction();
            Commande commande = new Commande();
            commande.setDateCommande(new Date("2022/12/01"));
            for (Produit p : produits) {
                commande.ajouterProduit(p);
            }
            session.save(commande);
            result = true;
        }
        session.getTransaction().commit();
        return result;
    }

    @Override
    public List<Commande> findAllOrders() {
        List<Commande> commandes = session
                .createQuery("from Commande")
                .list();
        return commandes;
    }

    @Override
    public List<Commande> findOrdersByDate(Date d) {
        List<Commande> commandes = session
                .createQuery("from Commande where dateCommande=:date")
                .setParameter("date", d).list();
        return commandes;
    }
}
