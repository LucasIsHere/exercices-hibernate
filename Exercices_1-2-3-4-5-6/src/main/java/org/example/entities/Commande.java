package org.example.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "commande")
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "code_commande")
    private int id;
    private Double total;
    @Temporal(TemporalType.DATE)
    private Date dateCommande;
    @ManyToMany
    @JoinTable(name = "composer",
            joinColumns = @JoinColumn(name = "code_commande"),
            inverseJoinColumns = @JoinColumn(name = "id"))
    private Set<Produit> produits = new HashSet<>();
    @OneToOne
    @JoinColumn(name = "adresse_id")
    private Adresse adresse;

    public Commande() {
    }

    public Commande(Double total, Date dateCommande) {
        this.total = total;
        this.dateCommande = dateCommande;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getTotal() {
        double total = 0;
        for (Produit p : getProduits()) {
            total += p.getPrix();
        }
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Set<Produit> getProduits() {
        return produits;
    }

    public void setProduits(Set<Produit> produits) {
        this.produits = produits;
    }

    public void ajouterProduit(Produit p) {
        this.produits.add(p);
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "id=" + id +
                ", total=" + total +
                ", dateCommande=" + dateCommande +
                ", produits=" + produits +
                '}';
    }
}
