package org.example.dao;

import org.example.entities.Commande;
import org.example.entities.Commentaire;
import org.example.entities.Image;
import org.example.entities.Produit;

import java.util.Date;
import java.util.List;

public interface IDAOProduit {

    Boolean createProduit(Produit p);

    Produit getProduit(int id);

    Boolean deleteProduit(Produit p);

    Boolean updateProduit(Produit p);

    List<Produit> getAllProduits();

    List<Produit> filterByPriceOver(Double prix);

    List<Produit> filterByDate(Date d1, Date d2);

    List fonction1Exo3(Date min, Date max) throws Exception;

    void fonction2Exo3(Integer stock);

    Long fonction1Exo4();

    Double fonction2Exo4();

    List fonction3Exo4(List<Object> marques);

    int fonction4Exo4(String marque);

    boolean addNewImage(Image img, int id);

    boolean addNewComment(Commentaire com, int id);

    List filterProductsByGrade(int note);

    boolean createCommande(List<Produit> produits);
    List<Commande> findAllOrders();
    List<Commande> findOrdersByDate(Date d);
}
