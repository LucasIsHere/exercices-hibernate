package org.example;

import org.example.entities.Produit;
import org.example.services.ServiceProduit;

import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {

        ServiceProduit test = new ServiceProduit();
        Scanner sc = new Scanner(System.in);

        // EXERCICE 1

//        // Création de 5 produits
//        test.createProduit(new Produit("Bic", "Stylo bleu", new Date(), 2.00, 300));
//        test.createProduit(new Produit("Bic", "Stylo vert", new Date(), 2.00, 300));
//        test.createProduit(new Produit("Bic", "Stylo rouge", new Date(), 2.00, 300));
//        test.createProduit(new Produit("Bic", "Stylo noir", new Date(), 2.00, 300));
//        test.createProduit(new Produit("Bic", "Stylo 4 couleurs", new Date(), 2.00, 300));
//
//        // Ajout de 3 produits avec dateAchat différentes
//        test.createProduit(new Produit("Aston Martin", "Valkyrie", new Date("2022/11/20"), 2500000.0, 10));
//        test.createProduit(new Produit("Tesla", "Model X", new Date("2022/11/21"), 141990.0, 100));
//        test.createProduit(new Produit("Mercedes AMG", "GT R", new Date("2022/11/22"), 127800.0, 110));
//
//        // Utilisation méthode getProduit
//        Produit produit2 = test.getProduit(2);
//        System.out.println(produit2);
//
//        // Utilisation méthode deleteProduit
//        test.deleteProduit(test.getProduit(3));
//
//        // Utilisation méthode updateProduit
//        Produit produit1 = test.getProduit(1);
//        produit1.setReference("Stylo bleu foncé");
//        produit1.setStock(200);
//        produit1.setPrix(1.5);
//        test.updateProduit(produit1);


        // EXERCICE 2

//        System.out.println("\nAfficher liste des produits :");
//        List<Produit> produits = test.getAllProduits();
//        for (Produit p : produits) {
//            System.out.println(p);
//        }
//
//        System.out.println("\nAfficher liste des produits avec prix > 100€ :");
//        List<Produit> produitsOver100 = test.filterByPriceOver(100.0);
//        for (Produit p : produitsOver100) {
//            System.out.println(p);
//        }
//
//        System.out.println("\nAfficher liste des produits achetés entre deux dates :");
//        Date date1 = new Date("2022/11/15");
//        Date date2 = new Date("2022/11/21");
//        List<Produit> produitsSellBetween = test.filterByDate(date1, date2);
//        for (Produit p : produitsSellBetween) {
//            System.out.println(p);
//        }


        // EXERCICE 3

//        System.out.println("\nAfficher liste des produits achetés entre deux dates (saisies par l'utilisateur) :");
//        System.out.print("\nDate 1 (format jj/mm/aaaa)? ");
//        String date = sc.nextLine();
//        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
//        System.out.print("Date 2 (format jj/mm/aaaa)? ");
//        date = sc.nextLine();
//        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
//
//        List listProduits = test.fonction1Exo3(date1, date2);
//        for (Object o : listProduits) {
//            System.out.println(o);
//        }

//        System.out.println("\nAfficher les produits dont le stock est inférieure à une valeur saisie par l'utilisateur :");
//        System.out.print("Valeur stock ? ");
//        int inputI = sc.nextInt();
//        test.fonction2Exo3(inputI);


        // EXERCICE 4-5

        Menu.main();
    }
}