package org.example;

import org.example.entities.Commande;
import org.example.entities.Commentaire;
import org.example.entities.Image;
import org.example.entities.Produit;
import org.example.services.ServiceProduit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Menu {

    public static void main() {

        boolean exit = false;

        do {

            ServiceProduit test = new ServiceProduit();

            System.out.println("\n1 - Exercice 1");
            System.out.println("2 - Exercice 2");
            System.out.println("3 - Exercice 3");
            System.out.println("4 - Exercice 4");
            System.out.println("5 - Exercice 5");
            System.out.println("6 - Exercice 6");
            System.out.println("0 - Exit");

            Scanner sc = new Scanner(System.in);
            System.out.print("\nQuel est votre choix ? ");
            int choix = sc.nextInt();

            switch (choix) {
                case 1:
                    System.out.println("\nExercice 1");
                    break;
                case 2:
                    System.out.println("\nExercice 2");
                    break;
                case 3:
                    System.out.println("\nExercice 3");
                    break;
                case 4:
                    System.out.println("\nExercice 4");

                    System.out.println("\n1. Question 1");
                    System.out.println("2. Question 2");
                    System.out.println("3. Question 3");
                    System.out.println("4. Question 4");
                    System.out.println("0. Retour");

                    System.out.print("\nQuestion à effectuer ? ");
                    choix = sc.nextInt();

                    switch (choix) {
                        case 1:
                            System.out.println("\nAfficher la valeur du stock des produits de la marque BIC.");
                            Long resultat = test.fonction1Exo4();
                            System.out.println("\nSomme des stocks des produits de la marque BIC = " + resultat + " unités");
                            break;
                        case 2:
                            System.out.println("\nCalculer le prix moyen des produits.");
                            Double moyennePrix = test.fonction2Exo4();
                            System.out.println("\nMoyenne des prix = " + moyennePrix + "€");
                            break;
                        case 3:
                            System.out.println("\nRécupérer la liste des produits des marques de voitures.");
                            List<Object> marquesVoiture = new ArrayList<>();
                            marquesVoiture.add("Aston Martin");
                            marquesVoiture.add("Tesla");
                            marquesVoiture.add("Mercedes AMG");
                            List<Produit> listProduits = test.fonction3Exo4(marquesVoiture);
                            listProduits.forEach(System.out::println);
                            break;
                        case 4:
                            System.out.println("\nSupprimer les produits de la marque BIC.");
                            String marque = "Bic";
                            int success = test.fonction4Exo4(marque);
                            System.out.println("\nNombres de produits supprimés : " + success);
                            break;
                        case 0:
                            break;
                        default:
                            System.out.println("\nErreur de saisie.");
                            break;
                    }
                    break;
                case 5:
                    System.out.println("\nExercice 5");

                    System.out.println("\n1. Ajouter une image");
                    System.out.println("2. Ajouter un commentaire");
                    System.out.println("3. Afficher produits par note min");
                    System.out.println("0. Retour");

                    System.out.print("\nQuestion à effectuer ? ");
                    choix = sc.nextInt();

                    switch (choix) {
                        case 1:
                            System.out.println("\nAjout de 3 images à des produits déjà existants.");
                            Image img1 = new Image("https://amsc-prod-cd.azureedge.net/-/media/aston-martin/images/default-source/models/valkyrie/valkyrie-retouched-nov21.jpg?mw=1980&rev=-1&hash=26AC755C251D9FBEAAEB0A230A3475E9");
                            test.addNewImage(img1, 6);

                            Image img2 = new Image("https://tesla-cdn.thron.com/delivery/public/image/tesla/8410774a-2d2c-4867-805d-9a549b9eac30/bvlatuR/std/4096x2561/Model-X-Range-Hero-Desktop-LHD");
                            test.addNewImage(img2, 7);

                            Image img3 = new Image("https://www.turbo.fr/sites/default/files/2020-07/736964.jpg");
                            test.addNewImage(img3, 8);
                            break;
                        case 2:
                            System.out.println("\nAjout de 3 commentaires à des produits déjà existants.");
                            Commentaire com1 = new Commentaire("Trop chère", new Date("2022/05/16"), 2);
                            test.addNewComment(com1, 6);

                            Commentaire com2 = new Commentaire("Trop électrique", new Date("2022/08/03"), 3);
                            test.addNewComment(com2, 7);

                            Commentaire com3 = new Commentaire("Qualité irréprochable", new Date("2021/09/24"), 5);
                            test.addNewComment(com3, 8);
                            break;
                        case 3:
//                            System.out.println("\nAfficher les produits avec une note >= 4");
//                            List<Produit> produits = test.filterProductsByGrade(4);
//                            for (Produit p : produits) {
//                                System.out.println(p);
//                            }
                            break;
                        case 0:
                            break;
                        default:
                            System.out.println("\nErreur de saisie.");
                            break;
                    }
                    break;
                case 6:
                    System.out.println("\nExercice 6 :");

                    System.out.println("\n1. Créer une commande");
                    System.out.println("2. Afficher la totalité des commandes");
                    System.out.println("3. Afficher uniquement les commandes du jour");
                    System.out.println("0. Retour");

                    System.out.print("\nAction à effectuer ? ");
                    choix = sc.nextInt();

                    switch (choix) {
                        case 1:
                            System.out.println("\nCréation de commande :");

                            // Afficher les produits disponibles
                            List<Produit> produitsDispos = test.getAllProduits();
                            System.out.println("\nListe des produits disponibles :");
                            for (Produit p : produitsDispos)
                                System.out.println(p);
                            System.out.println("");

                            // Ajouter les produits à la commande
                            List<Produit> produits = new ArrayList<>();
                            int idproduit;
                            do {
                                System.out.print("Id produit à ajouter dans votre commande (0 pour finaliser la commande)? ");
                                idproduit = sc.nextInt();
                                if (idproduit != 0) {
                                    Produit ptemp = test.getProduit(idproduit);
                                    if (ptemp != null)
                                        produits.add(test.getProduit(idproduit));
                                    else System.out.println("Le produit n'existe pas.");
                                } else System.out.println("Merci pour votre commande.");
                            } while (idproduit != 0);

                            // Créer la commande
                            boolean result = test.createCommande(produits);
                            if (result)
                                System.out.println("Commande créée avec succès !");
                            break;
                        case 2:
                            System.out.println("\nAfficher la totalité des commandes :");
                            List<Commande> commandes = test.findAllOrders();
                            for (Commande c : commandes)
                                System.out.println(c);
                            break;
                        case 3:
                            System.out.println("\nAfficher les commandes du jour :");
                            System.out.print("\nDate des commandes à afficher (format JJ/MM/AAAA) : ");

                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            sc.nextLine();
                            String stringDate = sc.nextLine();
                            Date date = null;
                            try {
                                date = sdf.parse(stringDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            List<Commande> commandesJour = test.findOrdersByDate(date);
                            for (Commande c : commandesJour)
                                System.out.println(c);
                            break;
                        case 0:
                            break;
                        default:
                            System.out.println("Erreur de saisie.");
                            break;
                    }
                    break;
                case 0:
                    System.out.println("\nFermeture du programme...");
                    exit = true;
                    break;
                default:
                    System.out.println("\nErreur de saisie.");
                    break;
            }
        } while (!exit);
    }
}
